## Install & Setup

Install & run React app:

```
$> cd react-ui

$> npm install

$> npm start
```

Install & run API server:

```
$> npm install

$> npm start
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

## App setup

More on this later, keep tuned!
